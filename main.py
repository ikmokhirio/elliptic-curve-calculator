def extended_euclidean_algorithm(a, b):
    s, old_s = 0, 1
    t, old_t = 1, 0
    r, old_r = b, a

    while r != 0:
        quotient = old_r // r
        old_r, r = r, old_r - quotient * r
        old_s, s = s, old_s - quotient * s
        old_t, t = t, old_t - quotient * t

    return old_r, old_s, old_t


def inverse_of(n, p):
    gcd, x, y = extended_euclidean_algorithm(n, p)
    assert (n * x + p * y) % p == gcd

    if gcd != 1:
        # Или n равно 0, или p не является простым.
        raise ValueError(
            '{} has no multiplicative inverse '
            'modulo {}'.format(n, p))
    else:
        return x % p


def add(xP, yP, xQ, yQ, p, a):
    m = 0

    if (xP != xQ) or (yP != yQ):
        m = ((yP - yQ) * inverse_of((xP - xQ), p)) % p
    else:
        m = ((3 * xP * xP + a) * inverse_of((2 * yP), p)) % p

    xR = (((m * m) - (xP + xQ)) % p)
    yR = ((m * (xP - xR) - yP) % p)
    return xR, yR


def multiplication(x, y, n, p, a):
    a, b = add(x, y, x, y, p, a)
    n -= 2
    for i in range(n):
        a, b = add(a, b, x, y, p, a)
    return a, b


answer = input("Multiplication? y/n").lower()

if answer == 'n':
    xP = int(input("P_X:"))
    yP = int(input("P_Y:"))

    xQ = int(input("Q_X:"))
    yQ = int(input("Q_Y:"))

    p = int(input("Modulo:"))
    a = int(input("A:"))

    x, y = add(xP, yP, xQ, yQ, p, a)

    print("X_R : {}".format(x))
    print("Y_R : {}".format(y))

elif answer == 'y':
    xP = int(input("P_X:"))
    yP = int(input("P_Y:"))

    p = int(input("Modulo:"))
    a = int(input("A:"))

    n = int(input("N:"))

    x, y = multiplication(xP, yP, n, p, a)

    print("X_R : {}".format(x))
    print("Y_R : {}".format(y))